# CSE 511A Intro AI Projects

**Five Projects, all from an Introduction to Artificial Intelligence Course Taken Fall 2017.**
* The course was very focused on the implementation of search and statistical algorithms through the gradual building of an autonomous agent that played Pacman. 
* The course began with implementations of pathfinding algorithms like A* Search and went through Bayes Nets and Decision Diagrams. Professor heavily based course off CS188 taught at Berkely

