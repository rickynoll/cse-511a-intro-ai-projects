import sys
from queue import PriorityQueue


class PriorityQueueWrapper(PriorityQueue):
    def __init__(self):
        PriorityQueue.__init__(self)
        self.counter = 0

    def put(self, item, priority):
        PriorityQueue.put(self, (priority, self.counter, item))
        self.counter += 1

    def get(self, *args, **kwargs):
        _, _, item = PriorityQueue.get(self, *args, **kwargs)
        return item


for line in sys.stdin:
    rows = line.split(';')
    dim = len(rows)
    matrix = []
    for row in rows:
        entries_array = row.split(',')
        for idx, entry in enumerate(entries_array):
            entries_array[idx] = int(entry)
        matrix.append(entries_array)
    queue = PriorityQueueWrapper()
    alreadySeen = []
    start_pos = (0, 0)
    actions = ['r', 'd']
    queue.put((start_pos, 0, None), 0)
    while not queue.empty():
        pos, path_value, action_just_taken = queue.get()

        if (pos[0] == dim and pos[1] == dim):
            print("Ending!!!")
            print(action_just_taken)
            break
        else:
            alreadySeen.append(pos)

        if action_just_taken != None:
            print(action_just_taken + ',')

        for action in actions:
            if pos in alreadySeen:
                if action is 'r' and pos[0] < dim - 1:
                    next_pos = (pos[0] + 1, pos[1])
                    next_pos_value = matrix[pos[1]][pos[0] + 1]
                    if next_pos not in alreadySeen:
                        successor = ((pos[0] + 1, pos[1]), path_value + next_pos_value, action)
                        priority_val = 1.0 / (path_value + next_pos_value)
                        queue.put(successor, priority_val)
                elif action is 'd' and pos[1] < dim - 1:
                    next_pos = (pos[0], pos[1] + 1)
                    next_pos_value = matrix[pos[1] + 1][pos[0]]
                    if next_pos not in alreadySeen:
                        successor = ((pos[0], pos[1] + 1), path_value + next_pos_value, action)
                        priority_val = 1.0 / (path_value + next_pos_value)
                        queue.put(successor, priority_val)



