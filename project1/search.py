# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first
    [2nd Edition: p 75, 3rd Edition: p 87]

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm
    [2nd Edition: Fig. 3.18, 3rd Edition: Fig 3.7].

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    # print "Start:", problem.getStartState()
    # print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    # print "Start's successors:", problem.getSuccessors(problem.getStartState())

    "*** YOUR CODE HERE ***"

    fringe = util.Stack();
    start = (problem.getStartState(), [], 0);
    fringePositions = [];
    alreadySeen = [];
    fringe.push(start);
    # until we reach the goal state, the loop will continue to visit nodes on the fringe
    while not fringe.isEmpty():
        currState, direction, cost = fringe.pop();
        # print currState, " current state";
        # print direction, " direction";
        # print cost, " cost";
        if problem.isGoalState(currState):
            return direction;
        else:
            alreadySeen.append(currState);
        for state in fringe.list:
            fringePositions.append(state[0]);
        for checkState, checkDirection, checkCost in problem.getSuccessors(currState):
            if ((checkState not in alreadySeen) and (checkState not in fringePositions)):
                currDirections = direction + [checkDirection];
                fringe.push((checkState, currDirections, checkCost));
    return None;


def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first.
    [2nd Edition: p 73, 3rd Edition: p 82]
    """
    # print "Start:", problem.getStartState()
    # print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    # print "Start's successors:", problem.getSuccessors(problem.getStartState())

    "*** YOUR CODE HERE ***"

    fringe = util.Queue();
    start = (problem.getStartState(), [], 0);
    fringePositions = [];
    alreadySeen = [];
    fringe.push(start);
    # until we reach the goal state, the loop will continue to visit nodes on the fringe
    while not fringe.isEmpty():
        currState, direction, cost = fringe.pop();
        # print currState, " current state";
        # print direction, " direction";
        # print cost, " cost";
        if problem.isGoalState(currState):
            return direction;
        else:
            alreadySeen.append(currState);
        for state in fringe.list:
            fringePositions.append(state[0]);
        for checkState, checkDirection, checkCost in problem.getSuccessors(currState):
            if ((checkState not in alreadySeen) and (checkState not in fringePositions)):
                currDirections = direction + [checkDirection];
                fringe.push((checkState, currDirections, checkCost));
    return None;


def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"
    queue = util.PriorityQueue();
    totalPathCost = 0;
    start = (0, problem.getStartState(), []);
    alreadySeen = [];
    queue.push(start, 0);
    while not queue.isEmpty():
        totalPathCost, currState, currDirection = queue.pop();
        # print totalPathCost, " total path cost";
        # print currState, " curr state 0";
        # print currDirection, " curr direction";
        if (problem.isGoalState(currState)):
            return currDirection;
        else:
            alreadySeen.append(currState);
        for checkState, checkDirection, checkCost in problem.getSuccessors(currState):
            if (checkState not in alreadySeen):
                successor = (totalPathCost + checkCost, checkState, currDirection + [checkDirection])
                queue.push(successor, checkCost)
    return []





def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"
    queue = util.PriorityQueue();
    start = (problem.getStartState(), [], 0);
    alreadySeen = set();
    queue.push(start, heuristic(problem.getStartState(), problem));
    while not queue.isEmpty():
        currState, currDirection, currCost = queue.pop();
        pathCost = problem.getCostOfActions(currDirection);
        heuristic_val = heuristic(currState, problem)
        # print totalPathCost, " total path cost";
        # print currState, " curr state 0";
        # print currDirection, " curr direction";
        if (problem.isGoalState(currState)):
            return currDirection;

        if (currState not in alreadySeen):
            alreadySeen.add(currState);
            for checkState, checkDirection, checkCost in problem.getSuccessors(currState):
                successor = (checkState, currDirection + [checkDirection], pathCost + checkCost + heuristic(checkState, problem))
                queue.push(successor, successor[2])
        elif currState in alreadySeen and pathCost + heuristic_val < currCost:
            queue.push((currState, currDirection, pathCost + heuristic_val), pathCost + heuristic_val)


    # queue = util.PriorityQueue();
    # totalPathCost = heuristic(problem.getStartState(), problem);
    # start = (totalPathCost, problem.getStartState(), []);
    # alreadySeen = [];
    # queue.push(start, heuristic(start[1], problem));
    # while not queue.isEmpty():
    #     currCost, currState, currDirection = queue.pop();
    #     # print totalPathCost, " total path cost";
    #     # print currState, " curr state 0";
    #     print currDirection, " curr direction";
    #     # totalPathCost = currCost - heuristic(currState, problem);
    #     # print 'Total forward path cost is', totalPathCost
    #     if (problem.isGoalState(currState)):
    #         # print 'Reached Goal!'
    #         return currDirection;
    #     alreadySeen.append(currState);
    #     for checkState, checkDirection, checkCost in problem.getSuccessors(currState):
    #         if (checkState not in alreadySeen):
    #             # newTuple = (checkState, checkCost)
    #             successor = (totalPathCost + checkCost + heuristic(checkState, problem), checkState,
    #                          currDirection + [checkDirection]);
    #             # print 'Child node', successor[1][0], 'has a total_cost of', totalPathCost, '+',
    #             queue.push(successor, successor[0]);
    # return [];

    # "Search the node that has the lowest combined cost and heuristic first."
    # "*** YOUR CODE HERE ***"
    # queue = util.PriorityQueue() # problem.getStartState() returns
    # start = (problem.getStartState(),[]) # ( ( (x,y), [] ), path )
    # alreadySeen = []
    # queue.push(start, 0)
    #
    # while not queue.isEmpty():
    #     current_state, path = queue.pop()
    #     total_path_cost = problem.getCostOfActions(path) - heuristic(current_state, problem)
    #
    #     if problem.isGoalState(current_state):
    #         return path
    #     else:
    #         alreadySeen.append(current_state[0])
    #
    #     for state, direction, cost in problem.getSuccessors(current_state):
    #         if state[0] not in alreadySeen:
    #             print direction, " direction"
    #             path_to_child = path + [direction]
    #             total_cost = problem.getCostOfActions(path_to_child) + heuristic(state, problem)
    #             queue.push((state, path_to_child), total_cost)
    #         # elif state in alreadySeen and total_cost > max([total_cost]):
    #         #     state = current_state
    # return []



# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
