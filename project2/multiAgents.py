# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util
import sys
from game import Agent


class ReflexAgent(Agent):
    """
        A reflex agent chooses an action at each choice point by examining
        its alternatives via a state evaluation function.

        The code below is provided as a guide.  You are welcome to change
        it in any way you see fit, so long as you don't touch our method
        headers.
    """

    # float('inf')
    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"
        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood().asList()
        newGhostStates = successorGameState.getGhostStates()

        "*** YOUR CODE HERE ***"
        if successorGameState.isWin():
            return float('inf')

        ghost_distances = [manhattanDistance(newPos, ghostState.getPosition()) for ghostState in newGhostStates]

        if action == Directions.STOP or any([distance < 2 for distance in ghost_distances]):
            return float('-inf')

        min_food_distance = min([manhattanDistance(newPos, food_pos) for food_pos in newFood])

        food_weight = 0
        capsule_weight = 0

        if currentGameState.getNumFood() > successorGameState.getNumFood():
            food_weight = 300

        if len(currentGameState.getCapsules()) > len(successorGameState.getCapsules()):
            capsule_weight = 600

        return successorGameState.getScore() - 5 * min_food_distance + food_weight + capsule_weight


        # if any([ghost_dist < 5 for ghost_dist in ghost_distances]):
        #     state_score = 5 * min(ghost_distances)
        #
        # new_food_list = newFood.asList()
        # min_dist = min([util.manhattanDistance(food_pos, newPos) for food_pos in new_food_list])
        # state_score += successorGameState.getScore() + (1 / min_dist) * 100
        #
        # if newPos in successorGameState.getCapsules():
        #     if any([ghost_dist < 3 for ghost_dist in ghost_distances]):
        #         state_score += 10000
        #     else:
        #         state_score += 5000
        #
        # if successorGameState.getNumFood() < currentGameState.getNumFood():
        #     state_score += 300
        #
        # return state_score

        # print 'newPos:', newPos
        # print 'newFood', newFood
        # print 'newScaredTimes', newScaredTimes
        # print ghostState.getPosition()
        # min_dist = float('inf')
        # newGhostPositions = [ghostState.getPosition() for ghostState in newGhostStates]
        # newFoodList = newFood.asList()
        # for food_pos in newFoodList:
        #     new_distance = util.manhattanDistance(newPos, food_pos)
        #     if min_dist > new_distance and newPos not in newGhostPositions:
        #         min_dist = new_distance
        #
        # print 'min_dist', min_dist
        # print 'Inverese min_dist', 1.0 / min_dist
        # if 1 / min_dist == 0:
        #     return float('inf')
        # else:
        #     return 1 / min_dist
        # food_distance = [util.manhattanDistance(newPos, food_pos) for food_pos in new_food_list]
        # ghost_distances_from_food = [util.manhattanDistance(ghostState.getPosition(), ) for ghostState in newGhostStates]
        # if food_distance < min_dist:
        #     inf_flag = False
        #     min_dist = food_distance
        #     closest_food_position = food_pos

        # for row_index, row in enumerate(newFood):
        #     for col_index, col in enumerate(row):
        #         # print 'Column', col
        #         if col is True:
        #             food_pos = (row_index, col_index)
        # print min(food_distance)
        # print 1.0/float('inf')
        # if inf_flag:
        #     print 'Returning inf'
        #     return float('inf')
        # elif 1/min_dist is 0:
        #     print 'Returning', 1
        #     return 1
        # print 'Returning 1/min_dist'
        # return 1/min_dist


def scoreEvaluationFunction(currentGameState):
    """
        This default evaluation function just returns the score of the state.
        The score is the same one displayed in the Pacman GUI.

        This evaluation function is meant for use with adversarial search agents
        (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
        This class provides some common elements to all of your
        multi-agent searchers.  Any methods defined here will be available
        to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

        You *do not* need to make any changes here, but you can if you want to
        add functionality to all your adversarial search agents.  Please do not
        remove anything, however.

        Note: this is an abstract class: one that should not be instantiated.  It's
        only partially specified, and designed to be extended.  Agent (game.py)
        is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          Directions.STOP:
            The stop direction, which is always legal

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        ind = self.maxValue(gameState, self.index, self.depth)
        return ind[1]


    def minValue(self, gameState, agentIndex, depth):
        if depth == 0:
            return (self.evaluationFunction(gameState), "Stop")
        minVal = sys.maxint
        moves = gameState.getLegalActions(agentIndex)
        if not moves:
            return self.evaluationFunction(gameState)
        for move in moves:
            if agentIndex == (gameState.getNumAgents() - 1):
                minVal = min(minVal, self.maxValue(gameState.generateSuccessor(agentIndex, move), 0, depth - 1)[0])
            else:
                minVal = min(minVal,
                             self.minValue(gameState.generateSuccessor(agentIndex, move), agentIndex + 1, depth))
        return minVal


    def maxValue(self, gameState, agentIndex, depth):
        if depth == 0:
            return (self.evaluationFunction(gameState), "Stop")
        max = -sys.maxint - 1
        maxMove = "South"
        moves = gameState.getLegalActions(agentIndex)
        if "Stop" in moves:
            moves.remove("Stop")
        for move in moves:
            nextStateVal = self.minValue(gameState.generatePacmanSuccessor(move), agentIndex + 1, depth)
            if nextStateVal > max:
                maxMove = move
                max = nextStateVal
        return (max, maxMove)


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        ind = self.maxValue(gameState, self.index, self.depth, -sys.maxint-1, sys.maxint)
        return ind[1]

    def minValue(self, gameState, agentIndex, depth, alpha, beta):
        if depth == 0:
            return (self.evaluationFunction(gameState), "Stop")
        minVal = sys.maxint
        moves = gameState.getLegalActions(agentIndex)
        if not moves:
            return self.evaluationFunction(gameState)
        for move in moves:
            if agentIndex == (gameState.getNumAgents() - 1):
                minVal = min(minVal, self.maxValue(gameState.generateSuccessor(agentIndex, move), 0, depth - 1, alpha,
                                                   beta)[0])
                if minVal <= alpha:
                    return minVal
                beta = max(beta, minVal)
            else:
                minVal = min(minVal,
                             self.minValue(gameState.generateSuccessor(agentIndex, move), agentIndex + 1, depth, alpha,
                                           beta))
                if minVal <= alpha:
                    return minVal
                beta = max(beta, minVal)
        return minVal

    def maxValue(self, gameState, agentIndex, depth, alpha, beta):
        if depth == 0:
            return (self.evaluationFunction(gameState), "Stop")
        maxVal = -sys.maxint - 1
        maxMove = "South"
        moves = gameState.getLegalActions(agentIndex)
        if "Stop" in moves:
            moves.remove("Stop")
        for move in moves:
            nextStateVal = self.minValue(gameState.generatePacmanSuccessor(move), agentIndex + 1, depth, alpha, beta)
            if maxVal >= beta:
                return (maxVal, maxMove)
            alpha = max(alpha, maxVal)
            if nextStateVal > maxVal:
                maxMove = move
                maxVal = nextStateVal
        return (maxVal, maxMove)


class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    ind = self.totalValue(gameState, self.index, self.depth)
    return ind[1]

  def totalValue(self, gameState, agentIndex, depth):
    moves = gameState.getLegalActions(agentIndex)
    if (depth == 0) or (not moves):
      return (self.evaluationFunction(gameState), "Stop")
    if agentIndex == 0:
      max = -sys.maxint - 1
      maxMove = None
      for move in moves:
        thisVal = max
        if agentIndex == (gameState.getNumAgents() - 1):
          thisVal = self.totalValue(gameState.generateSuccessor(agentIndex, move), 0, depth - 1)[0]
        else:
          thisVal = self.totalValue(gameState.generateSuccessor(agentIndex, move), agentIndex + 1, depth - 1)[0]
        if ((thisVal == max and maxMove == "Stop") or (thisVal > max)):
          maxMove = move
          max = thisVal
      return (max, maxMove)
    else:
      sumVal = 0.0
      probability = 1.0 / (len(moves))
      for move in moves:
        if (agentIndex == gameState.getNumAgents() - 1):
          sumVal += self.totalValue(gameState.generateSuccessor(agentIndex, move), 0, depth)[0]
        else:
          sumVal += self.totalValue(gameState.generateSuccessor(agentIndex, move), agentIndex + 1, depth)[0]
      return (sumVal * probability, None)




def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    if currentGameState.isWin():
        return sys.maxint

    pacman_pos = currentGameState.getPacmanPosition()
    ghost_states = currentGameState.getGhostStates()
    aggressive_ghosts = [manhattanDistance(pacman_pos, ghost_state.getPosition()) for ghost_state in ghost_states
                         if ghost_state.scaredTimer <= 0]

    # Could possibly alter ghost_distance_wieght with a multiple whose weight depends on how close the distance is
    # Doesn't take walls into account
    if any([distance < 2 for distance in aggressive_ghosts]):
        return -sys.maxint

    scared_ghosts = [(manhattanDistance(pacman_pos, ghost_state.getPosition()), ghost_state.scaredTimer)
                     for ghost_state in ghost_states if ghost_state.scaredTimer > 0]

    ghost_distance_weight = 0

    if any([distance_timer[0] < 6 for distance_timer in scared_ghosts]):
        ghost_distance_weight = -5 * min(scared_ghosts, key=lambda t: t[0])[0]

    food_list = currentGameState.getFood().asList()
    min_food_distance = min([manhattanDistance(pacman_pos, food_pos) for food_pos in food_list])

    food_weight = -2 * currentGameState.getNumFood() - 5 * min_food_distance # changed -0.2 to -2

    capsules = currentGameState.getCapsules()
    capsule_weight = 0
    if capsules:
        min_capsule_distance = min([manhattanDistance(pacman_pos, capsule) for capsule in capsules])
        capsule_weight = -200 * len(capsules) - 7.5 * min_capsule_distance
        if pacman_pos in capsules:
            capsule_weight += 500

    if pacman_pos in food_list:
        food_weight += 100

    return scoreEvaluationFunction(currentGameState) + food_weight + capsule_weight + ghost_distance_weight

    # legalMoves = currentGameState.getLegalActions()
    # for move in legalMoves:
    #     successorGameState = currentGameState.generatePacmanSuccessor(move)
    #     newPos = successorGameState.getPacmanPosition()
    #     newFood = successorGameState.getFood()
    #     newGhostStates = successorGameState.getGhostStates()
    #     newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
    #     newGhostPositions = [ghostState.getPosition() for ghostState in newGhostStates]
    #
    #     # Important measures
    #     # Ghost distance
    #     # food distance + number of food eaten
    #     #
    #
    # util.raiseNotDefined()


# Abbreviation
better = betterEvaluationFunction


class ContestAgent(MultiAgentSearchAgent):
    """
    Your agent for the mini-contest
  """

    def getAction(self, gameState):
        """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()
